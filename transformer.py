import sys
import pandas as pd
from rdflib import Graph, URIRef, Literal, Namespace
from rdflib.namespace import RDF, XSD
import os
from tqdm import tqdm
from concurrent.futures import ProcessPoolExecutor, as_completed
import logging

# Set up logging
logging.basicConfig(filename='triplification.log', level=logging.INFO, 
                    format='%(asctime)s %(levelname)s:%(message)s')

# Define namespaces
SAREF = Namespace("https://saref.etsi.org/core/")
EX = Namespace("http://example.org/")

def create_graph():
    g = Graph()
    g.bind("saref", SAREF)
    g.bind("ex", EX)
    return g

def process_row(index, row, columns, non_numeric_columns, log_skipped):
    try:
        g = Graph()
        timestamp = row['utc_timestamp']
        time_uri = EX[f"time/{index}"]

        # Add time instance
        g.add((time_uri, RDF.type, SAREF.Time))
        g.add((time_uri, SAREF.hasTimestamp, Literal(timestamp, datatype=XSD.dateTime)))

        for column in columns:
            if column in non_numeric_columns:
                continue

            if pd.notna(row[column]):
                try:
                    measurement_value = float(row[column])
                except ValueError:
                    if log_skipped:
                        logging.warning(f"Skipping non-numeric value in column {column} at row {index}")
                    continue

                device_uri = EX[f"device/{column}"]
                property_uri = EX[f"property/{column}"]
                measurement_uri = EX[f"measurement/{index}/{column}"]

                # Add device and property instances
                g.add((device_uri, RDF.type, SAREF.Device))
                g.add((property_uri, RDF.type, SAREF.Property))
                g.add((device_uri, SAREF.hasProperty, property_uri))

                # Add measurement instance
                g.add((measurement_uri, RDF.type, SAREF.Measurement))
                g.add((measurement_uri, SAREF.hasValue, Literal(measurement_value, datatype=XSD.float)))
                g.add((measurement_uri, SAREF.isMeasuredIn, Literal("kW", datatype=XSD.string)))
                g.add((measurement_uri, SAREF.relatesToProperty, property_uri))
                g.add((measurement_uri, SAREF.hasTimestamp, time_uri))

                # Link device to measurement
                g.add((device_uri, SAREF.hasMeasurement, measurement_uri))
        
        return g, None
    except Exception as e:
        logging.error(f"Error processing row {index}: {e}")
        return None, f"Error processing row {index}: {e}"

def add_triples(g, df, non_numeric_columns, log_skipped):
    columns = df.columns[2:]  # Skip the timestamp columns
    futures = []

    with ProcessPoolExecutor() as executor:
        for index, row in df.iterrows():
            futures.append(executor.submit(process_row, index, row, columns, non_numeric_columns, log_skipped))
        
        for future in tqdm(as_completed(futures), total=len(futures), desc="Processing rows"):
            result, error = future.result()
            if result:
                g += result
            if error:
                logging.error(error)

    return g

def main(csv_path, log_skipped):
    if not os.path.isfile(csv_path):
        logging.error(f"Error: The file {csv_path} does not exist.")
        sys.exit(1)

    try:
        # Read the dataset
        df = pd.read_csv(csv_path)
    except Exception as e:
        logging.error(f"Error reading CSV file: {e}")
        sys.exit(1)
    
    # Define known non-numeric columns
    non_numeric_columns = ['interpolated']

    try:
        # Create RDF graph
        g = create_graph()
        
        # Add triples to the graph
        g = add_triples(g, df, non_numeric_columns, log_skipped)
        
        # Serialize the graph to Turtle format
        g.serialize(destination='graph.ttl', format='turtle')
        logging.info("RDF graph has been successfully serialized to graph.ttl")
    except Exception as e:
        logging.error(f"Error during RDF graph creation or serialization: {e}")
        sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        print("Usage: python transformer.py path/to/dataset.csv [log_skipped]")
        sys.exit(1)
    
    csv_path = sys.argv[1]
    log_skipped = bool(int(sys.argv[2])) if len(sys.argv) == 3 else False # Log skipped rows with non-numeric values
    main(csv_path, log_skipped)

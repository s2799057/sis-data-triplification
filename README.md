# IoT Dataset Triplification

This project aims to transform an IoT dataset serialized as CSV into an RDF graph using the SAREF ontology. This process, known as data triplification, leverages Python and the RDFLib library.

## Dataset

The dataset used in this project is the Open Power System Data Household Data, specifically the file containing measurements every 60 minutes, entitled `household_data_60min_singleindex.csv`.

https://data.open-power-system-data.org/household_data/2020-04-15

## Ontology

The SAREF (Smart Applications REFerence) ontology is used to provide the classes and properties for creating the RDF graph. We are using SAREF version 3.1.1.

## Requirements

- Python 3.x
- pandas
- RDFLib
- tqdm
- concurrent.futures
- sys
- os
- logging

These dependencies can be installed using the `requirements.txt` file.

## Setup

1. **Clone the Repository:**
   ```sh
   git clone git@gitlab.utwente.nl:s2799057/sis-data-triplification.git
   cd sis-data-triplification

2. **Create a Virtual Environment:**
    ```sh    
    python -m venv env
    source env/bin/activate  # On Windows use `env\Scripts\activate`

3. **Install Dependencies:**
    ```sh    
    pip install -r requirements.txt

## Running the Script

- Ensure the household_data_60min_singleindex.csv file is in the correct location.

- Run the script with the path to your dataset:

    ```sh
    python transformer.py "path/to/household_data_60min_singleindex.csv"

- Example:

    ```sh
    python transformer.py "G:/My Drive/uTwente/BIT/Smart Industries/Assignments/triplification/sis-data-triplification/household_data_60min_singleindex.csv" 1

- The 1 at the end enables logging of skipped rows with non-numeric values. Use 0 or omit the argument to disable logging.

## Script Overview

- transformer.py: This script reads the IoT dataset, transforms it into an RDF graph using the SAREF ontology, and serializes the graph into Turtle format (graph.ttl). The script includes error handling and displays a progress bar during the transformation process.

## Error Handling

- The script checks if the file exists before proceeding.
- Errors during the reading of the CSV file, processing of rows, and creation/serialization of the RDF graph are captured and displayed.

## Output

The resulting RDF graph is serialized in Turtle format and saved as 'graph.ttl'.

## Project Structure
    .
    ├── transformer.py
    ├── requirements.txt
    ├── README.md
    ├── household_data_60min_singleindex.csv  # (not included in the repository)
    ├── graph.ttl  # (not included in the repository)
    └── triplification.log  # (not included in the repository)

## License

This project is licensed under the MIT License.